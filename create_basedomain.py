loadProperties('create_basedomain.properties')
readTemplate(domainTemplate)
 
def printInStyle(txt):
 print'-'*10 +txt
  
cd('Servers/AdminServer')
printInStyle('Give your custom AdminServer name')
set('Name',adminServerName)
 
printInStyle('Set the ListenAddress and ListenPort')
set('ListenAddress',adminServerListenaddr)
set('ListenPort', int(admlistenport))
 
# Security
printInStyle('Creating Password')
cd('/')
cd(weblogicdomainpasspath)
cmo.setPassword(adminPassword)
 
printInStyle('Setting StartUp Options')
setOption('OverwriteDomain', OverwriteDomain)
 
# Create Domain to File System
printInStyle('Writing Domain To File System')
# Change the path to your domain accordingly
writeDomain(domainHome)
closeTemplate()
# Exiting
print('Exiting now...')
exit()