readTemplate(TMPLATE)
cd('/')
cmo.setName(DOMAIN_NAME)
cd('Servers/AdminServer')
cmo.setListenAddress(ADMIN_LISTENADDRESS)
cmo.setListenPort(int(ADMIN_PORT))
cd( '/' )
cd( 'Security/'+DOMAIN_NAME+'/User/' +ADMIN_USER)
cmo.setPassword(ADMIN_PWD)
cd('/')
setOption("ServerStartMode", "prod")
setOption("OverwriteDomain", "true")

writeDomain( DOMAIN_DIR+'/'+DOMAIN_NAME )
closeTemplate()